import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  currentTab = 'list'

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  navigate(link: string) {
    this.currentTab = link;
    this.router.navigate([`home/${link}`]);
  }

}
