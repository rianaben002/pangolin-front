import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSelectModule} from '@angular/material/select';

export const materialModule = [
    MatInputModule,
    MatIconModule,
    MatSnackBarModule,
    MatSelectModule
]