import { Component, OnInit } from '@angular/core';
import { Pangolin } from 'src/app/@core/model/pangolin.model';
import { HttpService } from 'src/app/@core/services/http/http.service';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { DataService } from 'src/app/@core/services/data/data.service';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  pangolins!: Pangolin[];

  constructor(private httpService: HttpService, private _snackBar: MatSnackBar, private dataService: DataService) { }

  ngOnInit(): void {
    console.log('Inint');
    
    this.httpService.getListPangolin()
      .subscribe(res => {
        console.log(res);
        
        if(res.status) {
          console.log(res.data);
          
          this.pangolins = res.data;
        }
      })
      
  }

  addFriend(_id: string) {
    console.log("hoeé", _id);
    
    this.httpService.addFriend(_id)
      .subscribe(data => {
        if(data.status) {
          this.openSnackBar()
          this.dataService.pangolinUser.next(data.data);
          this.ngOnInit()
          console.log("updated ", data.data);
          
        }
      })
  }

  openSnackBar() {
    this._snackBar.open("Pangolin ajouté dans votre liste d' amis", 'ok', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }
}
