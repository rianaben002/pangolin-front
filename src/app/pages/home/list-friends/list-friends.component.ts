import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Pangolin } from 'src/app/@core/model/pangolin.model';
import { DataService } from 'src/app/@core/services/data/data.service';
import { HttpService } from 'src/app/@core/services/http/http.service';

@Component({
  selector: 'app-list-friends',
  templateUrl: './list-friends.component.html',
  styleUrls: ['./list-friends.component.scss']
})
export class ListFriendsComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  friends!: Pangolin[];
  constructor(private _snackBar: MatSnackBar, private dataService: DataService, private httpService: HttpService) { }

  ngOnInit(): void {
    this.dataService.pangolinUser
      .subscribe(p => {
        if(p) {
          this.friends = p.friends;
          console.log("Friends ", this.friends);
          
        }
      })
  }

  openSnackBar() {
    this._snackBar.open("Pangolin supprimé de votre liste d' amis", 'ok', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  removeFriend(id: string) {
    console.log("Ok");
    
    this.httpService.removeFriend(id)
      .subscribe(data => {
        console.log(data);
        if(data.status) {
          console.log('data : ', data.data);
          
          this.dataService.pangolinUser.next(data.data);
          this.openSnackBar();
        }
      })
  }

}
