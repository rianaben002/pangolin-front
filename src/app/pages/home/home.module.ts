import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { ListComponent } from './list/list.component';
import { NavbarComponent } from 'src/app/@shared/navbar/navbar.component';
import { UpdatePangolinComponent } from './update-pangolin/update-pangolin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { materialModule } from 'src/app/@shared/material.UI/material.module';
import { MatInputModule } from '@angular/material/input';
import { ListFriendsComponent } from './list-friends/list-friends.component';


@NgModule({
  declarations: [
    HomeComponent,
    ListComponent,
    NavbarComponent,
    UpdatePangolinComponent,
    ListFriendsComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    ...materialModule
  ]
})
export class HomeModule { }
