import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Pangolin } from 'src/app/@core/model/pangolin.model';
import { DataService } from 'src/app/@core/services/data/data.service';
import { HttpService } from 'src/app/@core/services/http/http.service';

@Component({
  selector: 'app-update-pangolin',
  templateUrl: './update-pangolin.component.html',
  styleUrls: ['./update-pangolin.component.scss'],
})
export class UpdatePangolinComponent implements OnInit {
  updateForm!: FormGroup;
  pangolin!: Pangolin;
  horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  roles = ['Guerrier', 'Alchimiste', 'Sorcier', 'Espions', 'Enchanteur'];

  constructor(
    private httpService: HttpService,
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.initForm();
    console.log('Hello');

    this.dataService.pangolinUser.subscribe((p) => {
      console.log('Pangolin in data service : ', p);

      if (p) {
        this.pangolin = p;
      } else {
        let token = localStorage.getItem('token');
        // console.log(token);

        if (token) {
          console.log('in token', token);

          this.httpService
            .refreshCurrentPangolin(token)
            .subscribe((data) => {
              console.log('data ', data);

              if (data.status) {
                this.pangolin = data.data;
              }
            })
            .unsubscribe();
        }
      }
    });
    this.initForm();
  }

  initForm(): void {
    this.updateForm = this.formBuilder.group({
      name: [this.pangolin?.name, Validators.required],
      username: [this.pangolin?.username, Validators.required],
      role: [this.pangolin?.role, Validators.required],
    });
  }

  updatePangolinInfo() {
    const username = this.updateForm.get('username')?.value;
    const name = this.updateForm.get('name')?.value;
    const role = this.updateForm.get('role')?.value;
    console.log(username, name, role);

    this.httpService.updatePangolin(username, name, role).subscribe((data) => {
      if (data.status) {
        this.dataService.pangolinUser.next(data.data);
        console.log('new data : ', data.data);
        this.openSnackBar()
      }
    });
  }

  openSnackBar() {
    this._snackBar.open('Information mis à jour', 'ok', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }
}
