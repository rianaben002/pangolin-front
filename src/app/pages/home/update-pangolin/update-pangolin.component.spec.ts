import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePangolinComponent } from './update-pangolin.component';

describe('UpdatePangolinComponent', () => {
  let component: UpdatePangolinComponent;
  let fixture: ComponentFixture<UpdatePangolinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdatePangolinComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePangolinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
