import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';
import { ListComponent } from './list/list.component';
import { UpdatePangolinComponent } from './update-pangolin/update-pangolin.component';
import { ListFriendsComponent } from './list-friends/list-friends.component';
import { AuthGuard } from 'src/app/@core/guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    // component: HomeComponent
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'list',
        canActivate: [AuthGuard],
        component: ListComponent,
      },
      {
        path: 'update-pangolin',
        canActivate: [AuthGuard],
        component: UpdatePangolinComponent,
      },
      {
        path: 'list-friends',
        canActivate: [AuthGuard],
        component: ListFriendsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
