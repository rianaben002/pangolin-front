import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  apiUrl = "http://localhost:3000/api/pangolin"

  constructor(private http: HttpClient) { }

  login(username: string, password: string): Observable<any> {
    return this.http.post<Observable<any>>(`${this.apiUrl}/login`, {
      username: username,
      password: password
    })
  }

  register(username: string, password: string, name: string, role: string): Observable<any> {
    return this.http.post<Observable<any>>(`${this.apiUrl}/register`, {
      username: username,
      password: password,
      name: name,
      role: role,
    })
  }

  getListPangolin(): Observable<any> {
    return this.http.get<Observable<any>>(`${this.apiUrl}/list`);
  }

  addFriend(id: string): Observable<any> {
    return this.http.post<Observable<any>>(`${this.apiUrl}/addFriend`, {
      pangolinId: id
    })
  }

  updatePangolin(username: string, name: string, role: string): Observable<any> {
    return this.http.post<Observable<any>>(`${this.apiUrl}/updatePangolin`, {
      username: username,
      name: name,
      role: role
    })
  }

  refreshCurrentPangolin(token: string): Observable<any> {
    console.log("Token : ", token);
        
    return this.http.post<Observable<any>>(`${this.apiUrl}/refreshCurrentPangolin`, {
      pangolinId: token
    })
  }

  removeFriend(_id: string): Observable<any> {
    return this.http.post<Observable<any>>(`${this.apiUrl}/deleteFriend`, {
      pangolinId: _id
    })
  }
 }
