import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Pangolin } from '../../model/pangolin.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  pangolinUser = new BehaviorSubject<Pangolin | null>(null);
  othersPangolin = new BehaviorSubject<Pangolin[] | null>(null);
  isConnected = new BehaviorSubject<boolean>(false);

  constructor() { }

  getToken() {
    return localStorage.getItem('token');
  }
}
