export type Role = "Guerrier" | "Alchimiste" | "Sorcier" | "Espions" | "Enchanteur";

export interface Pangolin {
    _id: string;
    name: string;
    username: string;
    password: string;
    role: Role;
    friends: Pangolin[];
}