import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { faFingerprint } from '@fortawesome/free-solid-svg-icons';
import { DataService } from 'src/app/@core/services/data/data.service';
import { HttpService } from 'src/app/@core/services/http/http.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  faFingerprint = faFingerprint;

  loginForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private httpService: HttpService, private router: Router, private dataService: DataService) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.loginForm = this.formBuilder.group({
      username: ['ben', Validators.required],
      password: ['123456', Validators.required]
    })
  }

  register() {
    this.router.navigate(['/auth/register'])
  }

  onLogin() {
    const username = this.loginForm.get('username')?.value;
    const password = this.loginForm.get('password')?.value;

    this.httpService.login(username, password)
      .subscribe(data => {
        if(data.status) {
          localStorage.setItem('token', data.token);
          console.log("Pangolin ", data.data);
          this.dataService.isConnected.next(true)
          this.dataService.pangolinUser.next(data.data)
          this.router.navigate(['/home']);
        }
      })
  }

}
